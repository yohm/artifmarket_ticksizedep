# Overview
A simulation program for an artificial market.

# How to build
1. Prepare prerequisites.
    - Build [json-cpp](http://jsoncpp.sourceforge.net/).
    - Download [boost library](http://www.boost.org/). You don't have to build it. Only header files are needed.
2. Edit Makefile so that INCLUDE and LIBS correctly specifies a path to json-cpp and boost on your machine.
3. make

# How to run
    ./am_tick_size.out _input.json
- Parameters are specified by a JSON file. Please see \_input.json 

