#ifndef __ORDER_HPP__
#define __ORDER_HPP__

#include <iostream>
#include <set>
#include "agent.hpp"
#include "json/json.h"

class Agent;

class Order {
public:
  explicit Order( long nPrice, Agent* pOwner, long nTime, bool bSell) :
  m_nPrice(nPrice), m_pOwner(pOwner), m_nTime(nTime), m_bSell(bSell), m_bContracted(false) {};
  virtual ~Order() {};
  const Json::Value ToJson() const {
    Json::Value out;
    out["Price"] = (int)m_nPrice;
    out["Owner"] = (int)(long)m_pOwner;
    out["Time"] = (int) m_nTime;
    out["Type"] = m_bSell ? "Sell" : "Buy";
    out["Contracted"] = m_bContracted;
    return out;
  }

  void Contract();  // update position of the owner
  long m_nPrice;
  Agent * m_pOwner;
  const long m_nTime;
  bool m_bSell;  // true: Sell, false: Buy
  bool m_bContracted; // after executed, it becomes true
};

// compare functor for buying boards
struct HigherPrice : public std::binary_function<Order*, Order*, bool> {
  bool operator()(Order* p1, Order* p2) const {
    if( p1->m_nPrice > p2->m_nPrice) {
      return true;
    }
    else if( p1->m_nPrice == p2->m_nPrice && p1->m_nTime < p2->m_nTime) {
      return true;
    }
    return false;
  }
};

// compare functor for selling boards
struct LowerPrice : public std::binary_function<Order*, Order*, bool> {
  bool operator()(Order* p1, Order* p2) const {
    if( p1->m_nPrice < p2->m_nPrice) {
      return true;
    }
    else if( p1->m_nPrice == p2->m_nPrice && p1->m_nTime < p2->m_nTime) {
      return true;
    }
    return false;
  }
};

#endif

