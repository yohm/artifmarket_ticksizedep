#include <iostream>
#include <string>
#include "order.cpp"
#include "agent.cpp"
#include "market.cpp"
#include "sf_calculator.cpp"
#include "app.cpp"

int main( int argc, char const *argv[] )
{
  if( argc != 2 ) {
    std::cerr << "Error! invalid argument" << std::endl
              << "Usage: <program> _input.json" << std::endl;
    return 1;
  }

  try {
    App zApp(argv[1]);
    std::cerr << "--- simulation parameters ---" << std::endl << zApp.Parameters() << std::endl;
    zApp.Run();
  } catch(const char* str) {
    std::cerr << "Unhandled exception:" << str << std::endl;
    return 1;
  }

  return 0;
}

