#ifndef __MARKET_HPP__
#define __MARKET_HPP__

#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <deque>
#include <assert.h>
#include "agent.hpp"
#include "json/json.h"

class Market {
public:
  explicit Market( long nTickSize, long nSaleRecordDuration )
    : m_nTickSize(nTickSize), m_nSalesRecordDuration(nSaleRecordDuration),
    m_nNumCancelled(0), m_nNumExecuted(0) {};
  virtual ~Market() {
    std::deque<Order*>::iterator it;
    for( it = m_dBuyingBoardsTimeOrderCache.begin(); it != m_dBuyingBoardsTimeOrderCache.end(); it++) {
      delete (*it);
    }
    for( it = m_dSellingBoardsTimeOrderCache.begin(); it != m_dSellingBoardsTimeOrderCache.end(); it++) {
      delete (*it);
    }
  };
  const Json::Value ToJson() const {
    Json::Value out;
    out["TickSize"] = (int)m_nTickSize;
    out["SalesAmountForT"] = (int)m_qSaledTime.size();
    out["Weight"] = m_dWeight;
    for( std::set<Order*>::iterator it = m_setSellingBoards.begin(); it != m_setSellingBoards.end(); it++) {
      out["SellingBoards"].append( (*it)->ToJson() );
    }
    for( std::set<Order*>::iterator it = m_setBuyingBoards.begin(); it != m_setBuyingBoards.end(); it++) {
      out["BuyingBoards"].append( (*it)->ToJson() );
    }
    return out;
  }

  // return best matching board
  // for the selling order, returns the highest buying board in the market
  // for the buying order, return the lowest selling board in the market
  // if no board in the market does not satisfy the price of pOrder, it returns NULL.
  Order* FindBestBoardFor( Order* pOrder );

  // insert pOrder as a board
  // price of the board is rounded up(/down) for selling(/buying) orders according to the tick size.
  void InsertBoard( Order* pOrder );

  // Execute contract for the best board in the market (yakujou)
  // return the contracted price
  // delete pOrder
  long Execute( Order* pOrder);

  void RecordSaledTime( long nTime );
  long SalesAmount();  // returns the sales amount for m_nSalesRecordDuration

  // Cancel board in the market
  // Also delete board objects in the market. Therefore, this must be called at each time step.
  // return the number of cancelled boards (0 or 1).
  long CancelBoard( long nExpirationTime );

  double GetWeight() { return m_dWeight; }
  void SetWeight( double d ) { m_dWeight = d; }

  size_t BoardSize() { return m_setBuyingBoards.size() + m_setSellingBoards.size(); }
  long HighestBuyingBoardPrice() {
    if( m_setBuyingBoards.empty() ) { return 0; }
    else { return (*m_setBuyingBoards.begin())->m_nPrice; }
  }
  long LowestSellingBoardPrice() {
    if( m_setSellingBoards.empty() ) { return 0; }
    else { return (*m_setSellingBoards.begin())->m_nPrice; }
  }

  long NumCancelled() { return m_nNumCancelled; }
  long NumExecuted() { return m_nNumExecuted; }
protected:
  // parameters
  const long m_nTickSize;  // Tick size

  std::queue<long> m_qSaledTime;  // store the times when a board is executed
  long m_nSalesRecordDuration;    // duration for which sales information is stored

  // weight
  double m_dWeight;

  // boards (ita)
  std::set<Order*, HigherPrice> m_setBuyingBoards;
  std::deque<Order*> m_dBuyingBoardsTimeOrderCache;
  std::set<Order*, LowerPrice> m_setSellingBoards;
  std::deque<Order*> m_dSellingBoardsTimeOrderCache;

  long m_nNumCancelled;
  long m_nNumExecuted;
};

#endif  // __MARKET_HPP__

