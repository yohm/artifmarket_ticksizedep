#include "agent.hpp"

// returns order
Order* Agent::MakeOrder( long nCurrentPrice, long nRefPrice, boost::random::mt19937* pRnd, long nTime) {
  // Calculate Target Price
  // TODO: tune up
  boost::random::normal_distribution<> normal_dist;
  double dR = 0.0;
  double factor = ( 1.0 / (m_adTypeWeight[0] + m_adTypeWeight[1] + m_adTypeWeight[2]));
  double dCurPrice = static_cast<double>(nCurrentPrice);
  double dFirst = m_adTypeWeight[0] * log( m_nFundPrice / dCurPrice );
  double dSecond = m_adTypeWeight[1] * log( dCurPrice / nRefPrice );
  double dThird = m_adTypeWeight[2] * m_dEps0 * normal_dist(*pRnd);
  dR = (dFirst + dSecond + dThird) * factor;
  double dTargetPrice = nCurrentPrice * exp(dR);

  // Determine Order Price
  double dOrderPrice = dTargetPrice + normal_dist(*pRnd) * m_dPoo;
  Order * pOrder = NULL;
  long nOrderPrice = static_cast<long>( dOrderPrice + 0.5 );

  // TODO: check the validity of this error-handling...
  if( nOrderPrice < 1 ) { nOrderPrice = 1; }
  else if( nOrderPrice > 10000 * m_nFundPrice ) { nOrderPrice = 10000*m_nFundPrice; }

  if( dOrderPrice >= dTargetPrice ) {
    pOrder = new Order( nOrderPrice, this, nTime, true);
  } else {
    pOrder = new Order( nOrderPrice, this, nTime, false);
  }
  return pOrder;
}

void Agent::UpdatePosition( long nStocks, long nCash) {
  m_nStock += nStocks;
  m_nCash += nCash;
}
