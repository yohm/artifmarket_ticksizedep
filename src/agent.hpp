#ifndef __AGENT_HPP__
#define __AGENT_HPP__

#include <iostream>
#include <cmath>
#include <vector>
#include "order.hpp"
#include "boost/random.hpp"
#include "json/json.h"

class Order;

class Agent {
public:
  explicit Agent(double adTypeWeight[3], long nFundPrice, double dEps0, double dPoo, long nTau) {
    for( int i=0; i<3; i++) {
      m_adTypeWeight[i] = adTypeWeight[i];
    }
    m_nStock = 0;
    m_nCash = 0;
    m_dEps0 = dEps0;
    m_dPoo = dPoo;
    m_nTau = nTau;
    m_nFundPrice = nFundPrice;
  };
  virtual ~Agent() {};
  long GetTau() { return m_nTau; }
  Order* MakeOrder(long nCurrentPrice, long nRefPrice, boost::random::mt19937* pRnd, long nTime);
  void UpdatePosition( long nStocks, long nCash);
  const Json::Value ToJson() const {
    Json::Value out;
    for( int i=0; i < 3; i++) { out["TypeWeight"].append(m_adTypeWeight[i]); }
    out["Eps0"] = m_dEps0;
    out["Poo"] = m_dPoo;
    out["Tau"] = (int)m_nTau;
    out["Stock"] = (int)m_nStock;
    out["Cash"] = (int)m_nCash;
    out["FundamentalPrice"] = (int) m_nFundPrice;
    return out;
  }
protected:
  // parameters
  double m_adTypeWeight[3];
  double m_dEps0;
  double m_dPoo; // noise factor of the order
  long m_nTau;      // duration for historical stock-price (default:10)
  long m_nFundPrice;

  // position of the agent
  long m_nStock;  // amount of stocks
  long m_nCash;   // cash
};

#endif //  __AGENT_HPP__

