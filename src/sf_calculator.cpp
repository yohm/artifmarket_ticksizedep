#include "sf_calculator.hpp"

const Json::Value StylizedFactCalculator::Calculate(const std::vector<long> & vnPrice, size_t nInitialSkip, size_t dt) {

  std::vector<double> vnReturn;
  std::vector<double> vnZeroExcludedRet;
  for( size_t i=nInitialSkip; (i+dt) < vnPrice.size(); i += dt) {
    double dRet = static_cast<double>(vnPrice[i+dt]) / vnPrice[i] - 1.0;
    vnReturn.push_back(dRet);
    if( vnPrice[i+dt] != vnPrice[i] ) {
      vnZeroExcludedRet.push_back( fabs(dRet) );
    }
  }

  Json::Value root;
  double dStdDev = 0.0;
  double dMean = 0.0;
  double dKurtosis = Kurtosis(vnReturn, dStdDev, dMean);

  root["StdDevOfReturn"] = dStdDev;
  root["KurtosisOfReturn"] = dKurtosis;
  root["MeanOfAbsZeroExcludedRet"] = Mean(vnZeroExcludedRet);;
  root["InitialSkip"] = (int)nInitialSkip;
  root["dt"] = (int)dt;

  return root;
}

double StylizedFactCalculator::Mean( const std::vector<double> & vdData) {
  double dSum = 0.0;
  for( size_t i=0; i < vdData.size(); i++) {
    dSum += vdData[i];
  }
  return dSum / vdData.size();
}

double StylizedFactCalculator::StdDev( const std::vector<double> & vdData, double& dMean) {
  dMean = Mean(vdData);
  double dSum = 0.0;
  for( size_t i=0; i < vdData.size(); i++) {
    double dDiff = vdData[i] - dMean;
    dSum += dDiff * dDiff;
  }
  return sqrt( dSum / (vdData.size() - 1.0) );
}

double StylizedFactCalculator::Kurtosis( const std::vector<double> & vdData, double& dStdDev, double& dMean) {
  dStdDev = StdDev(vdData, dMean);
  double s4 = dStdDev * dStdDev * dStdDev * dStdDev;
  double n = static_cast<double>( vdData.size() );
  double dSum = 0.0;
  for( size_t i=0; i < vdData.size(); i++) {
    double dDiff = (vdData[i] - dMean);
    dSum += dDiff * dDiff * dDiff * dDiff;
  }

  return (n*(n+1)) / ((n-1)*(n-2)*(n-3)) * dSum / s4 - 3*(n-1)*(n-1)/((n-2)*(n-3));
}
