#include "order.hpp"

void Order::Contract() {
  if( m_bSell ) {
    m_pOwner->UpdatePosition( -1, m_nPrice);
  }
  else {
    m_pOwner->UpdatePosition( 1, -m_nPrice);
  }
  m_bContracted = true;
}
