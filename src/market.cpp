#include "market.hpp"

Order* Market::FindBestBoardFor( Order* pOrder ) {
  if( pOrder->m_bSell ) {
    std::set<Order*>::iterator it = m_setBuyingBoards.begin();
    if( it != m_setBuyingBoards.end() && 
        ((*it)->m_nPrice >= pOrder->m_nPrice) ) {
      return *it;
    }
    else {
      return NULL;
    }
  }
  else {
    std::set<Order*>::iterator it = m_setSellingBoards.begin();
    if( it != m_setSellingBoards.end() &&
        ((*it)->m_nPrice <= pOrder->m_nPrice) ) {
      return *it;
    }
    else {
      return NULL;
    }
  }
}

long Market::Execute( Order* pOrder) {
  long nContractedPrice = 0;
  std::set<Order*>::iterator it;

  if( pOrder->m_bSell ) {
    it = m_setBuyingBoards.begin();
  } else {
    it = m_setSellingBoards.begin();
  }

  pOrder->m_nPrice = (*it)->m_nPrice;
  nContractedPrice = (*it)->m_nPrice;
  pOrder->Contract();
  (*it)->Contract();
  delete pOrder;
  // DO NOT delete *it because it is referenced by TimeOrderCache.
  // It will be deleted in 'CancelBoard' or destructor

  if( pOrder->m_bSell ) {
    m_setBuyingBoards.erase(it);
  } else {
    m_setSellingBoards.erase(it);
  }

  m_nNumExecuted++;
  return nContractedPrice;
}

void Market::RecordSaledTime( long nTime ) {
  m_qSaledTime.push(nTime);
  while( !m_qSaledTime.empty() && m_qSaledTime.front() < nTime - m_nSalesRecordDuration ) {
    m_qSaledTime.pop();
  }
}

long Market::SalesAmount() {
  return static_cast<long>(m_qSaledTime.size());
}

void Market::InsertBoard( Order* pOrder ) {
  if( pOrder->m_bSell ) {
    // ceil
    pOrder->m_nPrice = ( (pOrder->m_nPrice + m_nTickSize - 1) / m_nTickSize) * m_nTickSize;
    m_setSellingBoards.insert( pOrder );
    m_dSellingBoardsTimeOrderCache.push_back( pOrder );
  }
  else {
    // floor
    pOrder->m_nPrice = (pOrder->m_nPrice / m_nTickSize) * m_nTickSize;
    m_setBuyingBoards.insert( pOrder );
    m_dBuyingBoardsTimeOrderCache.push_back( pOrder );
  }
}

// return the number of cancelled boards
// NOTE: Assuming that at most one board is cancelled at each time step
// must be called at each time step
long Market::CancelBoard( long nExpirationTime ) {
  long nCancelled = 0;

  std::deque<Order*>::iterator it = m_dBuyingBoardsTimeOrderCache.begin();
  if( it != m_dBuyingBoardsTimeOrderCache.end() && (*it)->m_nTime == nExpirationTime ) {
    if( (*it)->m_bContracted == false ) {
      std::set<Order*>::iterator it2 = m_setBuyingBoards.find(*it);
      m_setBuyingBoards.erase(it2);
      nCancelled = 1;
    }
    delete (*it);
    m_dBuyingBoardsTimeOrderCache.pop_front();
    m_nNumCancelled += nCancelled;
    return nCancelled;
  }

  it = m_dSellingBoardsTimeOrderCache.begin();
  if( it != m_dSellingBoardsTimeOrderCache.end() && (*it)->m_nTime == nExpirationTime ) {
    if( (*it)->m_bContracted == false ) {
      std::set<Order*>::iterator it2 = m_setSellingBoards.find(*it);
      m_setSellingBoards.erase(it2);
      nCancelled = 1;
    }
    delete (*it);
    m_dSellingBoardsTimeOrderCache.pop_front();
    m_nNumCancelled += nCancelled;
    return nCancelled;
  }

  return 0;
}
