#ifndef __SF_CALCULATOR_HPP__
#define __SF_CALCULATOR_HPP__

#include <iostream>
#include <vector>
#include <cmath>
#include "json/json.h"

class StylizedFactCalculator {
public:
  StylizedFactCalculator() {};
  virtual ~StylizedFactCalculator() {};
  const Json::Value Calculate( const std::vector<long> & vnPrice, size_t nInitialSkip, size_t dt = 1);
protected:
  double Mean( const std::vector<double> & vData);
  double StdDev( const std::vector<double> & vData, double& dMean);
  double Kurtosis( const std::vector<double> & vdData, double& dStrDev, double& dMean);
  double dStdDevOfReturn;
  double dMeanOfZeroExcludedReturn;
};

#endif

