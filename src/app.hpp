#ifndef __APP_HPP__
#define __APP_HPP__

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <assert.h>
#include "market.hpp"
#include "sf_calculator.hpp"
#include "boost/random.hpp"
#include "json/json.h"


class InputParameters {
public:
  InputParameters( std::string sInputJsonPath );
  InputParameters( int argc, char const* argv[]);
  virtual ~InputParameters() {};
  const Json::Value ToJson() const {
    Json::Value v;
#define TO_JSON(var) v[#var] = var;
#define TO_JSON_I(var) v[#var] = static_cast<int>(var);

    TO_JSON(nAgents);
    TO_JSON(nMaxOrdes);
    TO_JSON(nTmax);
    TO_JSON(nMarkets);
    TO_JSON(nTickSizeA);
    TO_JSON(nTickSizeB);
    TO_JSON(dInitialMarketWeightA);
    TO_JSON(nFundPrice);
    TO_JSON_I(nMaxPrice);
    TO_JSON(nCancelTime);
    TO_JSON(nSalesRecordDuration);
    TO_JSON(nSeed);
    TO_JSON(nSeed);
    TO_JSON(nTauMax);
    for( int i=0; i < 3; i++) { v["AgentTypeWeightMax"].append(adAgentTypeWeightMax[i]); }
    TO_JSON(dEps0);
    TO_JSON(dPoo);
#undef TO_JSON
    return v;
  }

  int nAgents;     // number of agents (amax)
  int nMaxOrdes;   // max number of orders for each agent
  int nTmax;       // number of iterations
  int nMarkets;    // number of markets (1 or 2)
  int nTickSizeA;   // tick size of market A
  int nTickSizeB;   // tick size of market B
  double dInitialMarketWeightA; // initial market weight of A (must be 0 < w < 1)
                                // weight of B is (1-w)
  int nFundPrice;  // fundamental price
  long nMaxPrice;   // maximum price
  int nCancelTime; // (omax * amax)
  int nSalesRecordDuration;  // 5 days = 100 * nAgents (tm)
  unsigned int nSeed;       // random number seed

  // Parameters for Agents
  int nTauMax;        // duration for historical stock price
  double adAgentTypeWeightMax[3]; // maximum value of agent type (asg)
                                  // fundamental : 1.0, technical : 10.0, nosie: 1.0
  double dEps0;   // noise strength (eps0: 0.06)
  double dPoo;    // sigma of the noise factor for estimated price (poo: pf / 100.0 * 0.3)
};


class App {
public:
  explicit App( std::string sInputJsonPath ) : zParam(sInputJsonPath) { Initialize(); }
  virtual ~App() {
    for( size_t i=0; i < vpzMarkets.size(); i++) {
      delete vpzMarkets[i];
    }
    for( size_t i=0; i < vpzAgents.size(); i++) {
      delete vpzAgents[i];
    }
    delete pRnd;
  };
  const Json::Value Parameters() { return zParam.ToJson(); }
  void Run();
protected:
  void Initialize();

  const InputParameters zParam;

  std::vector<Market*> vpzMarkets;
  std::vector<Agent*> vpzAgents;
  boost::mt19937 * pRnd;
  std::vector<long> vnPrice;

  Market* FindMarketForMarketOrder( Order * pOrder ); // return true if market order matched
  void ProcessLimitOrder( Order* pOrder );
  long CancelBoards( long nExpirationTime ); //return number of cancelled boards
  void UpdateMarketWeights();
  void OutputResult( std::string strOutputJsonPath = "_output.json");
};

#endif // __APP_HPP__

