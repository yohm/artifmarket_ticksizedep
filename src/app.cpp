#include "app.hpp"

InputParameters::InputParameters( std::string sInputJsonPath ) {
  Json::Value root;
  Json::Reader reader;
  std::ifstream fin( sInputJsonPath.c_str() );
  bool bSuccess = reader.parse(fin, root);
  if( !bSuccess ) {
    std::cerr << "Error! Failed to parse json file : " << sInputJsonPath << std::endl;
    throw "Json parse error";
  }

#define SET_INT(var, key, def) (var) = root.isMember(key) ? root[(key)].asInt() : (def);
#define SET_DOUBLE(var, key, def) (var) = root.isMember(key) ? root[(key)].asDouble() : (def);

  SET_INT(nAgents, "NumAgents", 1000);
  SET_INT(nMaxOrdes, "MaxOrders", 10);
  SET_INT(nTmax, "TMax", 10000000);
  SET_INT(nMarkets, "NumMarkets", 2);
  SET_INT(nTickSizeA, "TickSizeA", 1000);
  SET_INT(nTickSizeB, "TickSizeB", 100);
  SET_DOUBLE(dInitialMarketWeightA, "InitialMarketWeightA", 0.8);
  SET_INT(nFundPrice, "FundamentalPrice", 1000000);
  SET_INT(nTauMax, "TauMax", 10000);
  SET_INT(nSeed, "_seed", 12345);
  SET_INT(nMaxPrice, "MaxPrice", 10000*nFundPrice);
  SET_INT(nSalesRecordDuration, "T_AB", 100*nAgents);
  SET_INT(nCancelTime, "TCancel", 20000);
  SET_DOUBLE(adAgentTypeWeightMax[0], "Weight1Max", 1.0);
  SET_DOUBLE(adAgentTypeWeightMax[1], "Weight2Max", 10.0);
  SET_DOUBLE(adAgentTypeWeightMax[2], "Weight3Max", 1.0);
  SET_DOUBLE(dEps0, "Sigma_E", 0.06);
  SET_DOUBLE(dPoo, "P_sigma", nFundPrice*0.003);

#undef SET_INT
#undef SET_DOUBLE
}

void App::Initialize() {

  pRnd = new boost::mt19937(zParam.nSeed);

  for( int i=0; i < zParam.nMarkets; i++) {
    Market* pMarket = NULL;
    if( i == 0 ) {
      pMarket = new Market(zParam.nTickSizeA, zParam.nSalesRecordDuration);
      pMarket->SetWeight(zParam.dInitialMarketWeightA);
    }
    else {
      pMarket = new Market(zParam.nTickSizeB, zParam.nSalesRecordDuration);
      pMarket->SetWeight(1.0 - zParam.dInitialMarketWeightA);  // CAUTION: only nMarket <= 2 is supported
    }
    vpzMarkets.push_back(pMarket);
  }

  for( long i = 0; i < zParam.nTauMax; i++) {
    vnPrice.push_back(zParam.nFundPrice);
  }

  // initialize agents
  boost::random::uniform_01<> uniform01;
  boost::random::uniform_int_distribution<> uniform_tau(1, zParam.nTauMax);
  for( int i=0; i < zParam.nAgents; i++) {
    double dWeight[3] = {0.0};
    for(int i=0; i < 3; i++) {
      dWeight[i] = zParam.adAgentTypeWeightMax[i] * uniform01(*pRnd);
    }
    // nTau distributes uniformly on [1 .. nTauMax]
    long nTau = uniform_tau(*pRnd);
    // long nTau = static_cast<long>(pRnd->genrand64_real1() * (zParam.nTauMax-1)) + 1;
    Agent * pzAgent = new Agent(dWeight, zParam.nFundPrice, zParam.dEps0, zParam.dPoo, nTau);
    vpzAgents.push_back(pzAgent);
  }
}

void App::Run() {
  long nOutputInterval = (zParam.nTmax / 10000 > 1) ? zParam.nTmax/10000 : 1;
  std::cerr << "OutputInterval: " << nOutputInterval << std::endl;
  long nCurrentPrice = zParam.nFundPrice;

  std::ofstream fout("price.txt");

  for( long t = 0; t < zParam.nTmax; t++) {

    // get order from agent
    Agent* pAgent = vpzAgents[t % zParam.nAgents];
    long nTau = pAgent->GetTau();
    long nRefPrice = vnPrice[vnPrice.size()-nTau];
    Order * pOrder = pAgent->MakeOrder( *(vnPrice.end()-1), nRefPrice, pRnd, t);

    if( t < zParam.nCancelTime ) {
      pOrder->m_bSell = ( pOrder->m_nPrice >= zParam.nFundPrice ) ? true : false;
    }

    if( pOrder->m_nPrice < 1 || pOrder->m_nPrice > zParam.nMaxPrice ) {
      std::cerr << pOrder->m_nPrice << std::endl;
      throw "invalid price";
    }

    // matching
    Market* pMarket = FindMarketForMarketOrder(pOrder);
    if( pMarket ) {
      nCurrentPrice = pMarket->Execute(pOrder);
      pMarket->RecordSaledTime(t);
      pOrder = NULL;
    }
    else {
      ProcessLimitOrder(pOrder);
    }

    if( t >= zParam.nCancelTime) {
      CancelBoards( t - zParam.nCancelTime );
    }

    if( t > zParam.nSalesRecordDuration ) {
      UpdateMarketWeights();
    }

    vnPrice.push_back(nCurrentPrice);

    if( t % nOutputInterval == 0 ) {
      fout << t << ' ' << nCurrentPrice << ' ';
      for( size_t i=0; i < vpzMarkets.size(); i++) {
        fout << vpzMarkets[i]->LowestSellingBoardPrice() << ' '
             << vpzMarkets[i]->HighestBuyingBoardPrice() << ' '
             << vpzMarkets[i]->GetWeight() << ' ';
      }
      fout << std::endl;
    }
  }

  fout.close();

  OutputResult();
}

Market* App::FindMarketForMarketOrder( Order* pOrder ) {
  // only the case for nMarket <= 2 is supported

  long nBestPrice = 0;
  Market* pBestMarket = NULL;
  boost::random::uniform_01<> uniform01;

  for( size_t i=0; i < vpzMarkets.size(); i++) {
    Order* pFoundBoard = vpzMarkets[i]->FindBestBoardFor(pOrder);
    if( pFoundBoard ) {
      if( pBestMarket ) {
        if( nBestPrice == pFoundBoard->m_nPrice ) {
          // select randomly following the weight
          pBestMarket = (uniform01(*pRnd) < vpzMarkets[0]->GetWeight()) ? vpzMarkets[0] :vpzMarkets[1];
        }
        else if( pOrder->m_bSell && nBestPrice < pFoundBoard->m_nPrice ) {
          pBestMarket = vpzMarkets[i];
          nBestPrice = pFoundBoard->m_nPrice;
        }
        else if( (!pOrder->m_bSell) && nBestPrice > pFoundBoard->m_nPrice ) {
          pBestMarket = vpzMarkets[i];
          nBestPrice = pFoundBoard->m_nPrice;
        }
      }
      else { // pBestMarket == NULL
        pBestMarket = vpzMarkets[i];
        nBestPrice = pFoundBoard->m_nPrice;
      }
    }
  }

  return pBestMarket;
}

void App::ProcessLimitOrder( Order* pOrder) {
  Market* pMarket = vpzMarkets[0];
  boost::random::uniform_01<> uniform01;
  // only nMarket <= 2 is supported
  if( vpzMarkets.size() > 1 ) {
    pMarket = (uniform01(*pRnd) < vpzMarkets[0]->GetWeight()) ? vpzMarkets[0] : vpzMarkets[1];
  }
  pMarket->InsertBoard(pOrder);
}

long App::CancelBoards( long nExpirationTime ) {
  long nNumCancelled = 0;
  for( size_t i=0; i < vpzMarkets.size(); i++) {
    nNumCancelled += vpzMarkets[i]->CancelBoard(nExpirationTime);
  }
  return nNumCancelled;
}

void App::UpdateMarketWeights() {
  long nTotal = 0;
  for( size_t i=0; i < vpzMarkets.size(); i++) {
    nTotal += vpzMarkets[i]->SalesAmount();
  }
  double dTotal = static_cast<double>(nTotal);
  for( size_t i=0; i < vpzMarkets.size(); i++) {
    double dWeight = vpzMarkets[i]->SalesAmount() / dTotal;
    vpzMarkets[i]->SetWeight(dWeight);
  }
}

void App::OutputResult( std::string strOutputJsonPath ) {

  // calculate stylized fact for several dt
  StylizedFactCalculator zSF;
  std::ofstream fout(strOutputJsonPath.c_str());
  Json::StyledWriter writer;
  Json::Value v;
  v.append( zSF.Calculate(vnPrice, zParam.nSalesRecordDuration, 1) );
  v.append( zSF.Calculate(vnPrice, zParam.nSalesRecordDuration, 10) );
  v.append( zSF.Calculate(vnPrice, zParam.nSalesRecordDuration, 20000) );
  Json::Value root;
  root["StylizedFact"] = v;

  // calculate cancel rate and execution rate
  double dTotal = static_cast<double>(zParam.nTmax);  // WARNING: Assuming Tmax == NumberOfOrders
  for( size_t i=0; i < vpzMarkets.size(); i++) {
    Json::Value market;
    market["ExecutionRate"] = vpzMarkets[i]->NumExecuted() / (dTotal + vpzMarkets[i]->NumCancelled());
    market["CancelRate"] = vpzMarkets[i]->NumCancelled() / (dTotal + vpzMarkets[i]->NumCancelled());
    market["Weight"] = vpzMarkets[i]->GetWeight();
    root["markets"].append(market);
  }

  fout << writer.write( root );
  fout.close();
}

