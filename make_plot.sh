#!/bin/sh
{
cat << EOF
set terminal postscript eps enhanced color
set title "time-price"
set output "price.eps"
unset key
plot "_input/price.txt" using 1:2 w l
EOF
} | gnuplot
convert price.eps price.png
