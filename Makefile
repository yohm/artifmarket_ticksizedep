CPP=g++
OPT=-O3 -Wall -DNDEBUG
#OPT=-O0 -Wall -pg
INCLUDE=-I ~/program/libs/jsoncpp-src-0.5.0/include -I ~/program/libs/boost_1_53_0
LIBS=~/program/libs/jsoncpp-src-0.5.0/libs/linux-gcc-4.2.1/libjson_linux-gcc-4.2.1_libmt.a

am_tick_size.out: src/*.cpp src/*.hpp Makefile
	$(CPP) $(OPT) $(INCLUDE) src/main.cpp $(LIBS) -o $@

clean:
	rm -f am_tick_size.out *~ *.bak
